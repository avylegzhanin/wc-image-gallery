const path = require('path');
const copy = require('copy-webpack-plugin');

module.exports = {
  entry: './source/main.js',
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist')
  },
  devServer: {
    contentBase: './dist',
    port: 9292,
    host: '10.3.0.215'
  },
  plugins: [
    new copy(
      [
        './source/index.html',
        './source/common.css',
        './source/favicon.ico',
        {
          from: './source/assets',
          to: './assets'
        },
        {
          from: './source/mocked-pictures',
          to: './mocked-pictures'
        }
      ],
      {
        to: './dist'
      }
    )
  ],
  devtool: 'source-map'
};