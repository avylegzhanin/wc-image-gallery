import Fetcher from './fetcher';

const flickrMethods = {
  'search': 'flickr.photos.search'
};

const config = {
  'per_page': '25'
};

export default class Flickr {
  static searchImagesByTags(tags, additionalParams = {}) {
    let params = Object.assign({}, config, additionalParams);

    return Fetcher.get({
      'method': flickrMethods['search'],
      'tags': tags,
      'privacy_filter': 1,
      'safe_search': 1,
      'content_type': 1,
      'media': 'photos',
      ...params
    }).then(response => {
      let {
        photos: {
          photo: images,
          page,
          pages
        }
      } = response;

      return { images, page, pages };
    });
  }

  static getImageUrl({ farm, server, id, secret }) {
    return `https://farm${farm}.staticflickr.com/${server}/${id}_${secret}.jpg`
  }
}