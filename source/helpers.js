export const qs = (selector, scope) => (scope || document).querySelector(selector);

export const qsa = (selector, scope) => (scope || document).querySelectorAll(selector);

export const on = (target, eventType, callback, useCapture = false) => {
  target.addEventListener(eventType, callback, useCapture);
};

export const delegate = (target, selector, eventType, callback, useCapture = false) => {
  const dispatchEvent = event => {
    let targetElement = event.target;
    let childNodes = qsa(selector, target);
    let hasMatch = Array.prototype.indexOf.call(childNodes, targetElement) >= 0;

    hasMatch && callback.call(targetElement, event);
  }

  on(target, eventType, dispatchEvent, useCapture);
};

export const ce = nodeString => document.createElement(nodeString);

export const cdf = () => document.createDocumentFragment();

export const random = (min, max) => Math.random() * (max - min) + min;

export const applyStyles = (targetNode, stylesString) => {
  let styleNode = ce('style');
  styleNode.textContent = stylesString;
  targetNode.appendChild(styleNode);
};