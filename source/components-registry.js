export default class ComponentsRegistry {
  constructor() {
    this.componentNames = [];
  }

  defineAll(components) {
    for (let component in components) {
      let { name, componentClass } = components[component];
      this.componentNames.push(name);
      customElements.define(name, componentClass);
    }

    return customElements.whenDefined(...this.componentNames);
  }
}