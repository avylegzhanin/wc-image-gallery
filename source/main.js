import ComponentsRegistry from './components-registry';
import M from './mediator.js';
import { on } from './helpers';
import components from './components';

const main = () => {
  const registry = new ComponentsRegistry();

  registry
    .defineAll(components)
    .then(() => M.trigger('init'));
};

on(window, 'load', main);