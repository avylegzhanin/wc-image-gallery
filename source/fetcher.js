import { API_KEY, SERVICE_URL } from './constants';

const XHR = new XMLHttpRequest();

const handleXHR = function (method, params) {
  XHR.open(method, getRequestUrl(params));
  XHR.send();

  return new Promise((resolve, reject) => {
    XHR.onreadystatechange = () => {
      if (XHR.readyState != 4) return;

      if (XHR.status != 200) {
        console.log(`${XHR.status}: ${XHR.staatusText}`);
      } else {
        resolve(JSON.parse(XHR.response));
      }
    }
  })
};

const stringifyRequestParams = params => {
  let resultString = '?';

  for (let key in params) {
    resultString += `${key}=${params[key]}&`;
  }

  return resultString.slice(0, -1);
};

const getRequestUrl = function (params) {
  params = Object.assign({}, {
    'api_key': API_KEY,
    'format': 'json',
    'nojsoncallback': 1
  }, params);

  return `${SERVICE_URL}${stringifyRequestParams(params)}`;
};

class Fetcher {
  static get(params) {
    return handleXHR('GET', params);
  }

  static test() {
    console.log('testing module');
  }
}

export default Fetcher;
