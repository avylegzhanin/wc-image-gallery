const handlers = {};

export default class M {
  static attach(triggerName, handler) {
    if (!handlers[triggerName]) handlers[triggerName] = [];

    handlers[triggerName].push(handler);
  }

  static detach() { /* code here... */ }

  static trigger(triggerName, parameters = []) {
    handlers[triggerName].forEach(f => f(...parameters));
  }
}