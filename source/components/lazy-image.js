import * as Vibrant from 'node-vibrant';
import { ce, qs, random, applyStyles } from '../helpers';

const getPlaceholderBackgroundCSS = Symbol();
const parseVibrantPallete = Symbol();
const onImageLoad = Symbol();

const styles = `
  :host {
    position: relative;
    display: block;
  }

  @media (min-width: 480px) {
    :host(:first-child) {
      grid-area: 1 / 1 / span 2 / span 2; 
    }

    :host(:nth-child(3n)) {
      grid-column: span 2;
    }
  }

  .placeholder {
    display: block;
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0;
    transition: opacity 600ms ease-in;
  }

  .placeholder--is-visible {
    opacity: 1;
  }

  .image {
    display: block;
    width: 100%;
    height: 100%;
    object-fit: cover;
    visibility: hidden;
  }

  .image--is-loaded {
    visibility: visible;
  }
`;

class LazyImage extends HTMLElement {
  constructor() {
    super();

    this.root = this.attachShadow({ mode: 'open' });
    applyStyles(this.root, styles);

    this.image = ce('img');
    this.image.classList.add('image');
    this.root.appendChild(this.image);

    const placeholder = ce('div');
    placeholder.classList.add('placeholder')
    this.root.appendChild(placeholder);

    const self = this;

    this.image.addEventListener('load', (event) => {
      self[getPlaceholderBackgroundCSS](this.image.getAttribute('src'))
        .then(additionalStyles => self[onImageLoad](additionalStyles));
    });
  }

  [onImageLoad](placeholderCSS) {
    let styleNode = qs('style', this.root);
    let placeholder = qs('.placeholder', this.root);

    styleNode.textContent += placeholderCSS;
    placeholder.classList.add('placeholder--is-visible');

    setInterval(() => {
      this.image.classList.add('image--is-loaded');
      placeholder.classList.remove('placeholder--is-visible');
    }, random(500, 1200));
  }

  [getPlaceholderBackgroundCSS](src) {
    let vibrant = new Vibrant(src);

    return vibrant
      .getPalette()
      .then(this[parseVibrantPallete])
      .then(colorRgbData => {
        return `.placeholder { background-color: rgba(${colorRgbData.join(',')},1) };`
      })
  }

  [parseVibrantPallete](pallete) {
    const defaultFlickrBlue = [0, 99, 219];
    let rgb = null;

    try {
      rgb = pallete.Vibrant._rgb;
    } catch (error) {
      rgb = defaultFlickrBlue;
    }

    return rgb.slice(0);
  }

  connectedCallback() {
    let imageUrl = this.getAttribute('url');

    this.image.setAttribute('src', imageUrl);
  }
}

export default {
  name: 'lazy-image',
  componentClass: LazyImage
}