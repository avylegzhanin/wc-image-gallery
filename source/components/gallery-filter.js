import { on, applyStyles, delegate, qsa } from '../helpers';
import _ from 'lodash';
import M from '../mediator';

const bindEvents = Symbol();
const render = Symbol();
const handleFilterClick = Symbol();
const findActiveFilters = Symbol();

const styles = `
  :host {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    width: 100%;
  }

  @media (min-width: 768px) {
    :host {
      width: auto;
    }
  }
  
  .filter {
    font-size: 18px;
    font-family: sans-serif;
    margin: 3px;
    padding: 5px 10px;
    border-radius: 50px;
    line-height: 1.2;
    border: 2px solid var(--flickr-purple);
    background-color: transparent;
    color: var(--flickr-purple);
    opacity: .9;
    cursor: pointer;
  }

  @media (min-width: 768px) {
    .filter {
      margin: 0 3px;
    }
  }

  .filter[aria-pressed="true"] {
    background-color: var(--flickr-purple);
    color: var(--white);
  }

  .filter:hover {
    opacity: 1;
  }
`;

class GalleryFilter extends HTMLElement {
  constructor() {
    super();
    this.root = this.attachShadow({ mode: 'open' });
    this[render]();
    this[bindEvents]();
  }

  [bindEvents]() {
    this[handleFilterClick] = this[handleFilterClick].bind(this);

    on(this.root, 'filtersSelected', _.debounce(event => {
      let activeFilters = this[findActiveFilters]();

      M.trigger('request', [activeFilters]);
    }, 500));

    delegate(this.root, '.filter', 'click', this[handleFilterClick]);

    M.attach('init', () => {
      let selectionEvent = new CustomEvent('filtersSelected');

      this.root.dispatchEvent(selectionEvent);
    })
  }

  [handleFilterClick](event) {
    let target = event.target,
      pressed = target.getAttribute('aria-pressed') === "true";

    target.setAttribute('aria-pressed', !pressed);

    let selectionEvent = new CustomEvent('filtersSelected');

    this.root.dispatchEvent(selectionEvent);
  }

  [findActiveFilters]() {
    return Array.prototype
      .map.call(
        qsa('.filter[aria-pressed="true"]', this.root),
        button => button.dataset.value
      ).join(',');
  }

  [render]() {
    applyStyles(this.root, styles);
    this.root.innerHTML += `
      <button class="filter" role="button" data-value="cars" aria-pressed="true">#cars</button>
      <button class="filter" role="button" data-value="animals">#animals</button>
      <button class="filter" role="button" data-value="nature">#nature</button>
      <button class="filter" role="button" data-value="art">#art</button>
      <button class="filter" role="button" data-value="people">#people</button>
      <button class="filter" role="button" data-value="space">#space</button>
      <button class="filter" role="button" data-value="music">#music</button>
    `;
  }
}

export default {
  name: 'gallery-filter',
  componentClass: GalleryFilter
};