import LazyImage from './lazy-image'
import ImageGallery from './image-gallery';
import GalleryFilter from './gallery-filter';
import GalleryLoader from './gallery-loader';

export default {
  GalleryFilter,
  GalleryLoader,
  ImageGallery,
  LazyImage
};