import { cdf, applyStyles } from '../helpers';
import M from '../mediator';

const bindEvents = Symbol();
const toggleLoader = Symbol();

const styles = `
  :host {
    display: block;
    position: fixed;
    padding: 5px 10px;
    left: 50%;
    bottom: 20px;
    transform: translate(-50%, 0);
    border-radius: 20px;
  }
  
  :host(.is-hidden) {
    transform: translate(-50%, 300px);
    transition: transform 700ms ease-in-out;
  }

  svg {
    margin-bottom: -3px;
  }

  svg circle {
    fill: var(--flickr-blue);
  }
`;

const svgLoader = `
  <!-- By Sam Herbert (@sherb), for everyone. More @ http://goo.gl/7AJzbL -->
  <svg width="80" height="20" viewBox="0 0 80 20" xmlns="http://www.w3.org/2000/svg" fill="#fff">
      <circle cx="10" cy="10" r="10">
          <animate attributeName="r" from="10" to="10"
                  begin="0s" dur="0.8s"
                  values="10;5;10" calcMode="linear"
                  repeatCount="indefinite" />
          <animate attributeName="fill-opacity" from="1" to="1"
                  begin="0s" dur="0.8s"
                  values="1;.5;1" calcMode="linear"
                  repeatCount="indefinite" />
      </circle>
      <circle cx="40" cy="10" r="5" fill-opacity="0.3">
          <animate attributeName="r" from="5" to="5"
                  begin="0s" dur="0.8s"
                  values="5;10;5" calcMode="linear"
                  repeatCount="indefinite" />
          <animate attributeName="fill-opacity" from="0.5" to="0.5"
                  begin="0s" dur="0.8s"
                  values=".5;1;.5" calcMode="linear"
                  repeatCount="indefinite" />
      </circle>
      <circle cx="70" cy="10" r="10">
          <animate attributeName="r" from="10" to="10"
                  begin="0s" dur="0.8s"
                  values="10;5;10" calcMode="linear"
                  repeatCount="indefinite" />
          <animate attributeName="fill-opacity" from="1" to="1"
                  begin="0s" dur="0.8s"
                  values="1;.5;1" calcMode="linear"
                  repeatCount="indefinite" />
      </circle>
  </svg>
`;

class GalleryLoader extends HTMLElement {
  constructor() {
    super();
    this[toggleLoader] = this[toggleLoader].bind(this);
    const root = this.attachShadow({ mode: 'open' });
    applyStyles(root, styles);
    root.innerHTML += svgLoader;
    this[bindEvents]();
  }

  [toggleLoader](state = false) {
    if (!state) {
      this.classList.add('is-hidden');
    } else {
      this.classList.remove('is-hidden');
    }
  }

  [bindEvents]() {
    M.attach('showLoader', () => {
      this[toggleLoader](true);
    });

    M.attach('hideLoader', () => {
      this[toggleLoader](false);
    });
  }
}

export default {
  name: 'gallery-loader',
  componentClass: GalleryLoader
}