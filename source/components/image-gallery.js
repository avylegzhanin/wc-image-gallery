import { on, qsa, cdf, ce, applyStyles } from '../helpers';
import Flickr from '../flickr-api';
import M from '../mediator.js';
import _ from 'lodash';

const generateColumns = Symbol();
const render = Symbol();
const renderImages = Symbol();
const bindEvents = Symbol();
const loadImagesBatch = Symbol();
const getCurrentPage = Symbol();
const handlePageScroll = Symbol();
const cleanUp = Symbol();

const styles = `
  :host {
    display: grid;
    margin: 0 auto;
    width: 95%;
    grid-gap: 10px;
    grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    grid-auto-rows: 250px 150px;
    grid-auto-flow: dense;
  }

  @media (min-width: 1024px) {
    :host {
      width: 80%;
    }
  }
`;

class ImageGallery extends HTMLElement {
  constructor() {
    super();
    this.filters = null;
    this.root = this.attachShadow({ mode: 'open' });
    applyStyles(this.root, styles);
    this[bindEvents]();
  }

  [bindEvents]() {
    this[renderImages] = this[renderImages].bind(this);
    this[getCurrentPage] = this[getCurrentPage].bind(this);
    this[loadImagesBatch] = this[loadImagesBatch].bind(this);
    this[handlePageScroll] = this[handlePageScroll].bind(this);

    M.attach('request', this[loadImagesBatch]);
    on(window, 'scroll', _.debounce(this[handlePageScroll], 300));
  }

  [generateColumns]() {
    let fragment = cdf();
    let columns = this.columns;

    for (let i = 0; i < columns; i += 1) {
      let column = ce('div');
      column.classList.add('column');
      fragment.appendChild(column);
    }

    return fragment;
  }

  [getCurrentPage]() {
    return this.getAttribute('page');
  }

  [handlePageScroll](event) {
    let documentHeight = document.documentElement.offsetHeight;
    let scrollHeight = window.screen.height + window.pageYOffset

    if (scrollHeight >= documentHeight) {
      let page = this[getCurrentPage]();
      this[loadImagesBatch](this.filters, { 'page': ++page });
    }
  }

  [loadImagesBatch](filters, params = {}) {
    // const fakeResponse = () => {
    //   return new Promise((resolve, reject) => {
    //     const response = {
    //       images: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18],
    //       page: 1,
    //       pages: 2
    //     };

    //     resolve(response);
    //   });
    // };

    // fakeResponse()
    //   .then(({ images, page, pages }) => {
    //     this.setAttribute('page', page);
    //     this.setAttribute('pages', pages);

    //     if (filters != null) {
    //       this[cleanUp]();
    //     }

    //     this[renderImages](images);
    //   });
    Flickr.searchImagesByTags(filters, params)
      .then(({ images, page, pages }) => {
        this.setAttribute('page', page);
        this.setAttribute('pages', pages);

        if (filters != null) {
          this[cleanUp]();
        }

        this[renderImages](images);
      });
  }

  [cleanUp]() {
    qsa('lazy-image', this.root)
      .forEach(lazyImage => this.root.removeChild(lazyImage))
  }

  [renderImages](images) {
    let fragment = cdf();

    /*  */
    const url = n => `/mocked-pictures/${n}.jpg`
    /*  */
    M.trigger('showLoader');

    images.forEach(imageData => {
      let lazyImage = ce('lazy-image');

      lazyImage.setAttribute('url', Flickr.getImageUrl(imageData));
      // lazyImage.setAttribute('url', url(imageData));
      fragment.appendChild(lazyImage);
    })

    this.root.appendChild(fragment);
    /* ??? end images loading helper */
    setTimeout(() => {
      M.trigger('hideLoader');
    }, 500);
  }
}

export default {
  name: 'image-gallery',
  componentClass: ImageGallery
}